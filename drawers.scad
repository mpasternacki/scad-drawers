
/* Drawer dimensions. X is face width, Y is face height, Z is depth. */
/* Full drawer. TODO: half v/h, quarter */
dim_full = [51, 51, 69];

chamfer = 3;

/* Wall (face / bottom / sides) thickness. Seems like a lot, but with
 * 0.6 nozzle & my settings it's 4 layers bottom & 3 layers sides. And
 * unchamfered cube with full dimensions of the inside doesn't stick
 * out of the outer chamfer. */
thickness = 1.5;

label = [20, 15, 1];

module drawer(dim=dim_full, side_h=undef, flat_bottom=false, no_back=false) {
    side_h_max = dim.y-2*chamfer;
    side_h = side_h == undef ? side_h_max : side_h;
    assert(side_h <= side_h_max, "Sides too high");
    assert(side_h >= 0, "Sides can't be negative");

    /* Face */
    linear_extrude(height=thickness) {
        polygon([ [chamfer, 0],
                  [dim.x - chamfer, 0],
                  [dim.x, chamfer],
                  [dim.x, dim.y - chamfer],
                  [dim.x - chamfer, dim.y],
                  [chamfer, dim.y],
                  [0, dim.y - chamfer],
                  [0, chamfer] ]);
    }

    /* Length */
    linear_extrude(height=dim.z) {
        polygon(concat(
                    /* Bottom */
                    [ [chamfer, 0],
                      [dim.x - chamfer, 0],
                      [dim.x, chamfer] ],
                    /* Side 1 */
                    side_h<=0 ? [] : [
                        [dim.x, chamfer + side_h],
                        [dim.x - thickness, chamfer + side_h],
                        ],
                    [ [dim.x - thickness, chamfer] ],
                    /* Empty bottom, if required */
                    flat_bottom ? [] : [
                        [dim.x - chamfer, thickness],
                        [chamfer, thickness],
                        ],
                    [ [thickness, chamfer] ],
                    /* Side 2 */
                    side_h<=0 ? [] : [
                        [thickness, chamfer + side_h],
                        [0, chamfer + side_h],
                        ],
                    [ [0, chamfer] ]
                    ));
    }


    /* Back */
    if ( !no_back ) {
        translate([0, 0, dim.z-thickness]) {
            linear_extrude(height=thickness) {
                polygon([ [chamfer, 0],
                          [dim.x - chamfer, 0],
                          [dim.x, chamfer],
                          [dim.x, chamfer + side_h],
                          [0, chamfer + side_h],
                          [0, chamfer] ]);
            }
        }
    }

    $drawer_inner_dim = dim - [thickness*2, thickness, thickness*2];
    $drawer_inner_chamfer = chamfer - thickness;
    $drawer_sides = side_h;
    $drawer_outer_chamfer = chamfer;
    translate([thickness, thickness, thickness])
        children();
}

drawer() {
    // # cube($drawer_inner_dim);
}
